local g = love.graphics
local p = love.physics

-- currently affects world boundary, screen size
local level = {
	width = 1280,
	height = 720,
	padding = 10, -- TODO: doesn't really fit here...
}

local _mt = {
	__index = {
		left   = function( self ) return 0 + self.padding end,
		right  = function( self ) return self.width - self.padding end,
		top    = function( self ) return 0 + self.padding end,
		bottom = function( self ) return self.height - self.padding end
	}
}

setmetatable( level, _mt )

local mech = {
	height = 48, width = 32,
	density = 1,
	horizontalForce = 100,
	jumpImpulse = 150,
	groundSensorPadding = 2,
	groundSensorHeight = 8
}

local mechState = {
	jump = false,
	inAir = true
}

-- the group all player-related physics objects belong to
-- they should never collide, therefore Box2D demands a negative index
local playerGroupIndex = -16

local keys = {
	left = 'a',
	right = 'd',
	jump = ' ',
	shoot = 'l', -- actually a mouse button!
}

local bgColor = { 104, 136, 248 }

local jumppack = {
	force = 200,
	maxFuel = 100,
	fuel = 100,
	consumptionRate = 80, -- per second
	activated = false,
	
	setActive = function( self, enabled ) self.activated = enabled end,
	isActivated = function( self ) return self.activated end,
	hasFuel = function( self ) return self.fuel > 0 end,
	isRunning = function( self ) return self:isActivated() and self:hasFuel() end,
	getForce = function( self ) return self.force end,
	getActiveForce = function( self )
		if self:isRunning() then
			return self.force
		else
			return 0
		end
	end,
	deplete = function( self, dt ) self.fuel = math.max( 0, self.fuel - self.consumptionRate * dt ) end,
	refuel = function( self ) self.fuel = self.maxFuel end
}

local maxVelocity = { x = 100 }
local mouse = { x = 0, y = 0 }

local shooting = false
local shotFired = false -- for semi-auto shots
local aimAngle = 0
local gun = {
	spawnRadius = 19,
	velocity = 2000,
	bulletShape = nil
}

local ammoType = {
	mass = 0.001
}

local bulletList = {
	first = nil,
	last = nil,
	count = 0,
	
	pushBack = function( self, bullet )
		bullet.previous = self.last
		bullet.next = nil 
		if not self.first then
			self.first = bullet
		end
		if self.last then
			self.last.next = bullet
		end
		self.last = bullet
		self.count = self.count + 1
	end,
	
	remove = function( self, bullet )
		if self.count <= 0 then
			return
		end
		
		if bullet.previous then
			bullet.previous.next = bullet.next
		else
			self.first = bullet.next
		end
		if bullet.next then
			bullet.next.previous = bullet.previous
		else
			self.last = bullet.previous
		end
		
		bullet.previous = nil
		bullet.next = nil
		
		self.count = self.count - 1
	end,
	
	iterate = function( self )
		function nextBullet( self, current )
			if current == nil then
				return self.first
			elseif current == self.last then
				return nil -- reached the end of the list, returning nil terminates the loop
			else
				return current.next
			end
		end
		
		return nextBullet, self, nil
	end
}

function bulletList:popFront()
	local bullet = self.first
	if bullet then
		self:remove( bullet )
	end
	return bullet
end

local maxBullets = 100

function begincollision( fixture1, fixture2, contact )
	if fixture1 == objects.playerMech.groundSensor or
			fixture2 == objects.playerMech.groundSensor then
		print( 'collision detected' )
		mechState.inAir = false
		jumppack:setActive( false )
		jumppack:refuel()
	end
	
	local ud1, ud2 = fixture1:getUserData(), fixture2:getUserData()
	if ud1 ~= nil and ud1.I_AM_A_BULLET then
		bulletList:remove( ud1 )
		ud1.fixture:destroy()
		ud1.body:destroy()
		-- TODO: trigger impact animation
	end
	if ud2 ~= nil and ud2.I_AM_A_BULLET then
		bulletList:remove( ud2 )
		ud2.fixture:destroy()
		ud2.body:destroy()
		-- TODO: trigger impact animation
	end
end

function endcollision( fixture1, fixture2, contact )
	if fixture1 == objects.playerMech.groundSensor or
			fixture2 == objects.playerMech.groundSensor then
		print( 'collision ended' )
		mechState.inAir = true
	end
end

function love.load()
	love.physics.setMeter( 64 ) -- one game meter equals 64 pixels
	world = love.physics.newWorld( 0, 9.81 * 64, true ) -- horizontal, vertical gravity, ?
	
	objects = {}
	
	objects.boundary = {}
	objects.boundary.body = p.newBody( world, 0, 0, 'static' )
	objects.boundary.shape = p.newChainShape( true, -- loop
			level:left(), level:top(),
			level:right(), level:top(),
			level:right(), level:bottom(),
			level:left(), level:bottom() )
	objects.boundary.fixture = p.newFixture(
			objects.boundary.body,
			objects.boundary.shape )
	
	objects.ground = {}
	objects.ground.body = love.physics.newBody( world, 650 * 0.5, 650 - 50 *0.5, 'static' )
	objects.ground.shape = love.physics.newRectangleShape( 650, 50 )
	objects.ground.fixture = love.physics.newFixture(
			objects.ground.body,
			objects.ground.shape )
	
	objects.playerMech = {}
	objects.playerMech.body = love.physics.newBody( world, 650 * 0.5, 650 * 0.5, 'dynamic' )
	objects.playerMech.body:setFixedRotation( true )
	objects.playerMech.body:setLinearDamping( 1 )
	objects.playerMech.shape = love.physics.newRectangleShape( mech.width, mech.height )
	objects.playerMech.fixture = p.newFixture(
			objects.playerMech.body,
			objects.playerMech.shape,
			mech.density ) -- add density to create mass
	objects.playerMech.fixture:setGroupIndex( playerGroupIndex )
	
	--objects.playerMech.fixture:setRestitution( 0.9 ) -- make it bouncy
	objects.playerMech.groundSensorShape = p.newRectangleShape(
			0,
			0.5 * mech.height - 0.5 * mech.groundSensorHeight + mech.groundSensorPadding,
			mech.width - 2 * mech.groundSensorPadding,
			mech.groundSensorHeight )
	objects.playerMech.groundSensor = p.newFixture(
			objects.playerMech.body,
			objects.playerMech.groundSensorShape,
			0 )
	objects.playerMech.groundSensor:setSensor( true )
	objects.playerMech.groundSensor:setGroupIndex( playerGroupIndex )
	
	objects.block1 = {}
	objects.block1.body = p.newBody( world, 200, 550, 'dynamic' )
	objects.block1.shape = p.newRectangleShape( 0, 0, 50, 100 )
	objects.block1.fixture = p.newFixture(
			objects.block1.body,
			objects.block1.shape,
			5 ) -- set high density
			
	objects.block2 = {}
	objects.block2.body = p.newBody( world, 200, 400, 'dynamic' )
	objects.block2.shape = p.newRectangleShape( 0, 0, 100, 50 )
	objects.block2.fixture = p.newFixture(
			objects.block2.body,
			objects.block2.shape,
			2 )
	
	-- need to initialize this here due to world scale being set
	gun.bulletShape = p.newRectangleShape( 10, 5 )
			
	love.graphics.setBackgroundColor( 191, 191, 191 )
	love.window.setMode( level.width, level.height )
	
	world:setCallbacks( begincollision, endcollision, nil, nil )
end

function spawnBullet( px, py, aimAngle )
	local bullet = {}

	local cos, sin = math.cos( aimAngle ), math.sin( aimAngle )
	
	bullet.body = p.newBody(
			world,
			px + cos * gun.spawnRadius,
			py + sin * gun.spawnRadius,
			'dynamic' )
	bullet.body:setBullet( true )
	bullet.body:setAngle( aimAngle )
	bullet.body:setLinearVelocity(
			cos * gun.velocity,
			sin * gun.velocity )
	
	bullet.shape = gun.bulletShape
	bullet.fixture = p.newFixture(
			bullet.body,
			bullet.shape )
	bullet.fixture:setGroupIndex( playerGroupIndex )
	bullet.body:setMassData( 0, 0, ammoType.mass, 0 )
	
	bullet.fixture:setUserData( bullet )
	-- set a 'flag' for identification during collision detection
	bullet.I_AM_A_BULLET = true
	
	return bullet
end

function love.update( dt )
	mouse.x, mouse.y = love.mouse.getPosition()
	world:update( dt )
	local px, py = objects.playerMech.body:getPosition()
	local dx, dy = mouse.x - px, mouse.y - py
	aimAngle = math.atan2( dy, dx )
	
	if shooting and not shotFired then
		shotFired = true
		local bullet = {}
		
		if bulletList.count >= maxBullets then
			local bullet = bulletList:popFront()
			bullet.fixture:destroy()
			bullet.body:destroy()
		end
		
		bullet = spawnBullet( px, py, aimAngle )
		bulletList:pushBack( bullet )
	end
	
	if love.keyboard.isDown( keys.right ) then
		objects.playerMech.body:applyForce( mech.horizontalForce, 0 )
	elseif love.keyboard.isDown( keys.left ) then
		objects.playerMech.body:applyForce( -mech.horizontalForce, 0 )
	end
	
	if mechState.jump and not mechState.inAir then
		objects.playerMech.body:applyLinearImpulse( 0, -mech.jumpImpulse )
		mechState.jump = false
	end
	
	if jumppack:isRunning() then
		objects.playerMech.body:applyForce( 0, -jumppack:getForce() )
		jumppack:deplete( dt )
	end
end

function love.keypressed( key )
	if key == keys.jump then
		if not mechState.inAir then
			mechState.jump = true
		else
			jumppack:setActive( true )
		end
	end
end

function love.keyreleased( key )
	if key == keys.jump then
		jumppack:setActive( false )
	end
end

function love.mousepressed( x, y, button )
	if button == keys.shoot then
		shooting = true
	end
end

function love.mousereleased( x, y, button )
	if button == keys.shoot then
		shooting = false
		shotFired = false
	end
end

function love.draw()
	g.setColor( bgColor )
	g.polygon( 'fill', objects.boundary.body:getWorldPoints( objects.boundary.shape:getPoints() ) )
	
	g.setColor( 72, 160, 14 )
	g.polygon( 'fill', objects.ground.body:getWorldPoints( objects.ground.shape:getPoints() ) )
	
	g.setColor( 193, 47, 14 )
	g.polygon( 'fill', objects.playerMech.body:getWorldPoints( objects.playerMech.shape:getPoints() ) )
	
	g.setColor( 50, 50, 50 )
	g.polygon( 'fill', objects.block1.body:getWorldPoints( objects.block1.shape:getPoints() ) )
	g.polygon( 'fill', objects.block2.body:getWorldPoints( objects.block2.shape:getPoints() ) )
	
	g.setColor( 255, 0, 0 )
	for bullet in bulletList:iterate() do
		g.polygon( 'fill', bullet.body:getWorldPoints( bullet.shape:getPoints() ) )
	end
	
	g.setColor( 255, 255, 0 )
	local mode = 'line'
	if mechState.inAir then
		mode = 'line'
	else
		mode = 'fill'
	end
	g.polygon( mode, objects.playerMech.body:getWorldPoints( objects.playerMech.groundSensorShape:getPoints() ) )
	
	g.setColor( 255, 255, 0 )
	local mechX, mechY = objects.playerMech.body:getPosition()
	g.line( mechX, mechY, mouse.x, mouse.y )
	g.circle( 'line', mechX, mechY, gun.spawnRadius )
	g.setColor( 0, 255, 0 )
	g.line( mechX, mechY, mechX + math.cos( aimAngle ) * gun.spawnRadius, mechY + math.sin( aimAngle ) * gun.spawnRadius )
	
	g.setColor( 0, 0, 0 )
	if jumppack:isActivated() then
		mode = 'fill'
	else
		mode = 'line'
	end
	g.rectangle( mode, 5, 5, 10, 10 )
	
	g.setColor( 255, 0, 0 )
	if jumppack:isRunning() then
		mode = 'fill'
	else
		mode = 'line'
	end
	g.rectangle( mode, 20, 5, 10, 10 )
	g.print( jumppack.fuel, 35, 5 )
	
	g.setColor( 0, 255, 255 )
	if shooting then
		mode = 'fill'
	else
		mode = 'line'
	end
	g.rectangle( mode, 5, 20, 10, 10 )
	g.print( "Bullets: "..bulletList.count, 35, 25 )
end
