# The Mech Project

Dependencies:

* [L�VE](http://www.love2d.org) >= 0.9.0

## Controls

*   **A/D** -- move left/right
*   **Spacebar** -- jump (on the ground), fire jump jets (in the air)
*   **Mouse** -- aim
*   **Left mouse button** -- shoot
